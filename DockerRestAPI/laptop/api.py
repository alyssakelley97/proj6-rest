# Laptop Service
from flask import request
from flask import Flask

from flask_restful import Resource, Api
import pymongo
from pymongo import MongoClient
import os

# Instantiate the app
app = Flask(__name__)
api = Api(app)

# Same as proj5
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

class Laptop(Resource):
    """
    This was the starter function provided in the skeleton code used as a reference
    for how our classes and functions should be structured. 

    This function will not be used.
    """
    def get(self):
        return {
            'Laptops': ['Mac OS', 'Dell', 
            'Windozzee',
        'Yet another laptop!',
        'Yet yet another laptop!'
            ]
        }

# LIST ALL FUNCTIONALITY
class listAll(Resource):
	# "http://<host:port>/listAll" should return all open and close times in the database
    def get(self):
        """
        This request returns all of the open and close times in the database. 
        The way the numbers are represented are in a standard, defalt JSON manner.

        To access this reporting, go to: http://localhost:5001/listAll

        I chose to have it just go through and list all since the project spec
        did not mention a min/max number to display. This request also does
        not have the ability to specify a top=# to display that amount.

        Note: You must have information entered in the database in order to have 
        the listAll functionality displaying properly. To input information into the
        database, you will need to access http://localhost:5010 and enter your info
        into the chart and click on the "submit" button.
        """
        items = db.tododb.find() # all of the items retrieved from the database
        all_items = [item for item in items]

        return {
            'open_times': [item['all_info_for_open'] for item in all_items],
            'close_times': [item['all_info_for_close'] for item in all_items]
        }

class listAllCSV(Resource):
	# "http://<host:port>/listAll/csv" should return all open and close times in CSV format
    def get(self):
        """
        This request returns all of the open and close times in the database in CSV format. 
        This means the way the information is represented in spaced out by commas. 
        EX: opentime1, closetime1, opentime2, closetime2, etc...

        To access this reporting, go to: http://localhost:5001/listAll/csv

        I chose to have it just go through and list all since the project spec
        did not mention a min/max number to display. This request also does
        not have the ability to specify a top=# to display that amount.

        Note: You must have information entered in the database in order to have 
        the listAll functionality displaying properly. To input information into the
        database, you will need to access http://localhost:5010 and enter your info
        into the chart and click on the "submit" button.
        """
        items = db.tododb.find()

        info_as_csv = ""
        for item in items:
            info_as_csv += item['all_info_for_open'] + " , " + item['all_info_for_close'] + " , "

        return info_as_csv

class listAllJSON(Resource):
	# "http://<host:port>/listAll/json" should return all open and close times in JSON format
    def get(self):
        """
        This request returns all of the open and close times in the database in JSON format. 
        This is the same as the default format for listAll.

        To access this reporting, go to: http://localhost:5001/listAll/json

        I chose to have it just go through and list all since the project spec
        did not mention a min/max number to display. This request also does
        not have the ability to specify a top=# to display that amount.

        Note: You must have information entered in the database in order to have 
        the listAll functionality displaying properly. To input information into the
        database, you will need to access http://localhost:5010 and enter your info
        into the chart and click on the "submit" button.
        """
        # amount_length = len('all_info_for_open')

        items = db.tododb.find()
        all_items = [item for item in items]

        return {
            'open_times': [item['all_info_for_open'] for item in all_items],
            'close_times': [item['all_info_for_close'] for item in all_items]
        }

# LIST OPEN FUNCTIONALITY
class listOpen(Resource):
	# "http://<host:port>/listOpenOnly" should return open times only
    def get(self):
        """
        This request returns all of the open times in the database. 
        The way the numbers are represented are in a standard, defalt JSON manner.

        To access this reporting, go to: http://localhost:5001/listOpenOnly

        I chose to have it just go through and list all since the project spec
        did not mention a min/max number to display. This request also does
        not have the ability to specify a top=# to display that amount. If you
        would like to specify displaying only the top x amount of open time, 
        you can use the listOpenOnly/csv or the listOpenOnly/json options.

        Note: You must have information entered in the database in order to have 
        the listAll functionality displaying properly. To input information into the
        database, you will need to access http://localhost:5010 and enter your info
        into the chart and click on the "submit" button.
        """
        items = db.tododb.find()

        return {
            'open_times': [item['all_info_for_open'] for item in items]
        }
                  
class listOpenCSV(Resource):
	# "http://<host:port>/listOpenOnly/csv" should return open times only in CSV format
    def get(self):
        """
        This request returns all of the open times in the database in CSV format. 
        This means the way the information is represented in spaced out by commas. 
        EX: opentime1, opentime2, etc...

        To access this reporting, go to: http://localhost:5001/listAll/csv

        This functionality allows the user to specify how many data points is 
        displayed to them. They can do this as so: listOpenOnly/csv?top=3. This
        means only the top 3 open times are going to be represented as a csv 
        on the page. If the user does not expicilty enter a top value, then all
        of the open values will be represented on the page as a csv.

        Note: You must have information entered in the database in order to have 
        the listOpenOnly/csv functionality displaying properly. To input information into the
        database, you will need to access http://localhost:5010 and enter your info
        into the chart and click on the "submit" button.
        """
        top_query_len = request.args.get("top") # This will be if the user does something like: ?top=3
        if (top_query_len == None): # if the user did not specify how many top entries they want to see
            top_query_len = len('all_info_for_open') # then display all of the open entries

        items = db.tododb.find().sort("openTime", pymongo.ASCENDING).limit(int(top_query_len)) # spec specified in ascending order

        info_as_csv = ""
        for item in items:
            info_as_csv += item['all_info_for_open'] + " , "

        return info_as_csv

class listOpenJSON(Resource):
	# "http://<host:port>/listOpenOnly/json" should return open times only in JSON format
    def get(self):
        """
        This request returns all of the open in the database in JSON format. 
        This is the same as the default format for listAll.

        To access this reporting, go to: http://localhost:5001/listOpenOnly/json

        This functionality allows the user to specify how many data points is 
        displayed to them. They can do this as so: listOpenOnly/json?top=3. This
        means only the top 3 open times are going to be represented as a json
        on the page. If the user does not expicilty enter a top value, then all
        of the open values will be represented on the page in a json format.

        Note: You must have information entered in the database in order to have 
        the listOpenOnly/json functionality displaying properly. To input information into the
        database, you will need to access http://localhost:5010 and enter your info
        into the chart and click on the "submit" button.
        """
        top_query_len = request.args.get("top") # This will be if the user does something like: ?top=3
        if (top_query_len == None):
            top_query_len = len('all_info_for_open')

        items = db.tododb.find().sort("openTime", pymongo.ASCENDING).limit(int(top_query_len))

        return {
            'open_times': [item['all_info_for_open'] for item in items]
        }

    
# LIST CLOSE FUNCTIONALITY
class listClose(Resource):
	# "http://<host:port>/listCloseOnly" should return close times only
    def get(self):
        """
        This request returns all of the close times in the database. 
        The way the numbers are represented are in a standard, defalt JSON manner.

        To access this reporting, go to: http://localhost:5001/listCloseOnly

        I chose to have it just go through and list all since the project spec
        did not mention a min/max number to display. This request also does
        not have the ability to specify a top=# to display that amount. If you
        would like to specify displaying only the top x amount of open time, 
        you can use the listCloseOnly/csv or the listCloseOnly/json options.

        Note: You must have information entered in the database in order to have 
        the listAll functionality displaying properly. To input information into the
        database, you will need to access http://localhost:5010 and enter your info
        into the chart and click on the "submit" button.
        """
        items = db.tododb.find()

        return {
            'close_times': [item['all_info_for_close'] for item in items]
        }

class listCloseCSV(Resource):
	# "http://<host:port>/listCloseOnly/json" should return close times only in JSON format# 
    def get(self):
        """
        This request returns all of the close times in the database in CSV format. 
        This means the way the information is represented in spaced out by commas. 
        EX: closetime1, closetime2, etc...

        To access this reporting, go to: http://localhost:5001/listCloseOnly/csv

        This functionality allows the user to specify how many data points is 
        displayed to them. They can do this as so: listCloseOnly/csv?top=3. This
        means only the top 3 close times are going to be represented as a csv 
        on the page. If the user does not expicilty enter a top value, then all
        of the close values will be represented on the page as a csv.

        Note: You must have information entered in the database in order to have 
        the listCloseOnly/csv functionality displaying properly. To input information into the
        database, you will need to access http://localhost:5010 and enter your info
        into the chart and click on the "submit" button.
        """
        top_query_len = request.args.get("top") # This will be if the user does something like: ?top=3
        if (top_query_len == None):
            top_query_len = len('all_info_for_close')

        items = db.tododb.find().sort("closeTime", pymongo.ASCENDING).limit(int(top_query_len))

        info_as_csv = ""
        for item in items:
            info_as_csv += item['all_info_for_close'] + " , "

        return info_as_csv


class listCloseJSON(Resource):
	# "http://<host:port>/listOpenOnly/json" should return open times only in JSON format
    def get(self):
        """
        This request returns all of the close in the database in JSON format. 
        This is the same as the default format for listAll.

        To access this reporting, go to: http://localhost:5001/listCloseOnly/json

        This functionality allows the user to specify how many data points is 
        displayed to them. They can do this as so: listCloseOnly/json?top=3. This
        means only the top 3 open times are going to be represented as a json
        on the page. If the user does not expicilty enter a top value, then all
        of the open values will be represented on the page in a json format.

        Note: You must have information entered in the database in order to have 
        the listCloseOnly/json functionality displaying properly. To input information into the
        database, you will need to access http://localhost:5010 and enter your info
        into the chart and click on the "submit" button.
        """
        top_query_len = request.args.get("top") # This will be if the user does something like: ?top=3
        if (top_query_len == None):
            top_query_len = len('all_info_for_close') 

        items = db.tododb.find().sort("closeTime", pymongo.ASCENDING).limit(int(top_query_len))

        return {
            'close_times': [item['all_info_for_close'] for item in items]
        }  

# Create routes
# Another way, without decorators
# example: api.add_resource(class_name, string_of_what_link_looks_like)
api.add_resource(Laptop, '/') # Exmaple one, this is not used

api.add_resource(listAll, '/listAll')
api.add_resource(listAllCSV, '/listAll/csv')
api.add_resource(listAllJSON, '/listAll/json')

api.add_resource(listOpen, '/listOpenOnly')
api.add_resource(listOpenCSV, '/listOpenOnly/csv')
api.add_resource(listOpenJSON, '/listOpenOnly/json')

api.add_resource(listClose, '/listCloseOnly')
api.add_resource(listCloseCSV, '/listCloseOnly/csv') 
api.add_resource(listCloseJSON, '/listCloseOnly/json')    

# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
