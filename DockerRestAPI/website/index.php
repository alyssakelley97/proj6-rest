<html>
    <head>
        <title>CIS 322 - REST-api - Display Times</title>
    </head>

    <body>
        <!-- Example -->
        <h1>List of laptops</h1>
            <?php
            $json = file_get_contents('http://laptop-service/');
            $obj = json_decode($json);
              $laptops = $obj->Laptops;
            foreach ($laptops as $l) {
                echo "<li>$l</li>";
            }
            ?>

        <!-- All of the times -->
        <h1>List of All</h1>
            <?php
            $json = file_get_contents('http://laptop-service/listAll');
            $obj = json_decode($json);
              $open_time = $obj->open_times;
              $close_time = $obj->close_times;
            echo "Open times: \n\n";
            // echo "This is $open_time";
            foreach ($open_time as $l) {
                echo "<li>$l</li>";
            }
            echo "Close times: \n\n";
            // echo "This is $close_time";
            foreach ($close_time as $l) {
                echo "<li>$l</li>";
            }
            ?>

        <!-- All of the times - CSV-->
        <h1>List of All in CSV format</h1>
            <?php
            echo "Open and close times in CSV format: \n\n";
            echo file_get_contents('http://laptop-service/listAll/csv');
            ?>

        <!-- All of the times - JSON -->
        <h1>List of All in JSON format</h1>
            <?php
            $json = file_get_contents('http://laptop-service/listAll/json');
            $obj = json_decode($json);
              $open_time = $obj->open_times;
              $close_time = $obj->close_times;
            echo "Open times in JSON format: \n\n";
            // echo "This is $open_time";
            foreach ($open_time as $l) {
                echo "<li>$l</li>";
            }
            echo "Close times in JSON format: \n\n";
            // echo "This is $close_time";
            foreach ($close_time as $l) {
                echo "<li>$l</li>";
            }
            ?>

        <!-- Open Times ONLY -->
        <h1>List of Open Times</h1>
            <?php
            $json = file_get_contents('http://laptop-service/listOpenOnly');
            $obj = json_decode($json);
              $open_time = $obj->open_times;
            echo "Open times: \n\n";
            // echo "This is $open_time";
            foreach ($open_time as $l) {
                echo "<li>$l</li>";
            }
            ?>

        <!-- Open Times ONLY - CSV-->
        <h1>List of Open Times in CSV format</h1>
            <?
            echo "Open times in CSV format:\n\n";
            echo file_get_contents('http://laptop-service/listOpenOnly/csv');
            ?>

        <!-- Open Times ONLY - JSON-->
        <h1>List of Open Times in JSON format</h1>
            <?php
            $json = file_get_contents('http://laptop-service/listOpenOnly/json');
            $obj = json_decode($json);
              $open_time = $obj->open_times;
            echo "Open times in JSON format \n\n";
            // echo "This is $open_time";
            foreach ($open_time as $l) {
                echo "<li>$l</li>";
            }
            ?>

        <!-- Close Times ONLY -->
        <h1>List of Close Times</h1>
            <?php
            $json = file_get_contents('http://laptop-service/listCloseOnly');
            $obj = json_decode($json);
              $close_time = $obj->close_times;
            echo "Close times:  \n\n";
            // echo "This is $open_time";
            foreach ($close_time as $l) {
                echo "<li>$l</li>";
            }
            ?>

        <!-- Close Times ONLY - CSV-->
        <h1>List of Close Times in CSV format</h1>
            <?php
            echo "Close times in CSV format: \n\n";
            echo file_get_contents('http://laptop-service/listCloseOnly/csv');
            ?>

        <!-- Close Times ONLY -JSON -->
        <h1>List of Close Times in JSON format</h1>
            <?php
            $json = file_get_contents('http://laptop-service/listCloseOnly/json');
            $obj = json_decode($json);
              $close_time = $obj->close_times;
            echo "Close times in JSON format: \n\n";
            // echo "This is $open_time";
            foreach ($close_time as $l) {
                echo "<li>$l</li>";
            }
            ?>

    </body>
</html>
