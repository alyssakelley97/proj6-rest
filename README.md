# Project 6: Brevet time calculator service

Alyssa Kelley - alyssak@uoregon.edu

I worked on this assignment with Anne Glickenhause. No code was copied, but ideas, concepts, and logic was shared.


## How to use this application:

You will need to first access the web port for the application. This will be accessed as: http://localhost:5010
Once you are on this page, you will be able to enter information into the chart displayed. Once you have finished 
entering your information, you need to click the "submit" button. If you want to see what you have just submitted,
you can click the "display" button. This is all functionality that was implemented in project 5 which is represented
in the DockerMongo subdirectory. 

The new functionality that was added is reporting that is available on port 5001. You can access this as: http://localhost:5001/<insert_reporting_here>
You have the following options to choose from in regards to how you want the open and close times reported to you on the webapge:

## listAll

    http://localhost:5010/listAll
        This request returns all of the open and close times in the database. 
        The way the numbers are represented are in a standard, defalt JSON manner.

        I chose to have it just go through and list all since the project spec
        did not mention a min/max number to display. This request also does
        not have the ability to specify a top=# to display that amount.

## listAll/csv

    http://localhost:5010/listAll/csv
        This request returns all of the open and close times in the database in CSV format. 
        This means the way the information is represented in spaced out by commas. 
        EX: opentime1, closetime1, opentime2, closetime2, etc...

        I chose to have it just go through and list all since the project spec
        did not mention a min/max number to display. This request also does
        not have the ability to specify a top=# to display that amount.

## listAll/json

    http://localhost:5010/listAll/json
        This request returns all of the open and close times in the database in JSON format. 
        This is the same as the default format for listAll.

        I chose to have it just go through and list all since the project spec
        did not mention a min/max number to display. This request also does
        not have the ability to specify a top=# to display that amount.

## listOpenOnly

    http://localhost:5010/listOpenOnly
        This request returns all of the open times in the database. 
        The way the numbers are represented are in a standard, defalt JSON manner.

        I chose to have it just go through and list all since the project spec
        did not mention a min/max number to display. This request also does
        not have the ability to specify a top=# to display that amount. If you
        would like to specify displaying only the top x amount of open time, 
        you can use the listOpenOnly/csv or the listOpenOnly/json options.

## listOpenOnly/csv

    http://localhost:5010/listOpenOnly/csv
        This request returns all of the open times in the database in CSV format. 
        This means the way the information is represented in spaced out by commas. 
        EX: opentime1, opentime2, etc...

        This functionality allows the user to specify how many data points is 
        displayed to them. 

        They can do this as so: http://localhost:5010/listOpenOnly/csv?top=3

        This means only the top 3 open times are going to be represented as a csv 
        on the page. If the user does not expicilty enter a top value, then all
        of the open values will be represented on the page as a csv. If
        the user enters a larger top amount than what it in the database, then 
        all of the items in the database will display to get as close to that top
        number as possible.

## listOpenOnly/json

    http://localhost:5010/listOpenOnly/json
        This request returns all of the open in the database in JSON format. 
        This is the same as the default format for listAll.

        This functionality allows the user to specify how many data points is 
        displayed to them. 

        They can do this as so: http://localhost:5010/listOpenOnly/json?top=5

        This means only the top 5 open times are going to be represented as a json
        on the page. If the user does not expicilty enter a top value, then all
        of the open values will be represented on the page in a json format. If
        the user enters a larger top amount than what it in the database, then 
        all of the items in the database will display to get as close to that top
        number as possible.

## listCloseOnly

    http://localhost:5010/listCloseOnly
        This request returns all of the close times in the database. 
        The way the numbers are represented are in a standard, defalt JSON manner.

        I chose to have it just go through and list all since the project spec
        did not mention a min/max number to display. This request also does
        not have the ability to specify a top=# to display that amount. If you
        would like to specify displaying only the top x amount of open time, 
        you can use the listCloseOnly/csv or the listCloseOnly/json options.

## listCloseOnly/csv

    http://localhost:5010/listCloseOnly/csv
        This request returns all of the close times in the database in CSV format. 
        This means the way the information is represented in spaced out by commas. 
        EX: closetime1, closetime2, etc...

        This functionality allows the user to specify how many data points is 
        displayed to them. 

        They can do this as so: http://localhost:5010/listCloseOnly/csv?top=6

        This means only the top 6 close times are going to be represented as a csv 
        on the page. If the user does not expicilty enter a top value, then all
        of the close values will be represented on the page as a csv. If
        the user enters a larger top amount than what it in the database, then 
        all of the items in the database will display to get as close to that top
        number as possible.

## listCloseOnly/json

    http://localhost:5010/listCloseOnly/json
        This request returns all of the close in the database in JSON format. 
        This is the same as the default format for listAll.

        To access this reporting, go to: http://localhost:5001/listCloseOnly/json

        This functionality allows the user to specify how many data points is 
        displayed to them. 

        They can do this as so: http://localhost:5010/listCloseOnly/json?top=4

        This means only the top 4 close times are going to be represented as a json
        on the page. If the user does not expicilty enter a top value, then all
        of the close values will be represented on the page in a json format. If
        the user enters a larger top amount than what it in the database, then 
        all of the items in the database will display to get as close to that top
        number as possible.
       


## Functionality that was specified and added as explained above:

This project has following four parts. Change the values for host and port according to your machine, and use the web browser to check the results.

* A RESTful service to expose what is stored in MongoDB. 
    * "http://<host:port>/listAll" should return all open and close times in the database
    * "http://<host:port>/listOpenOnly" should return open times only
    * "http://<host:port>/listCloseOnly" should return close times only

* Two different representations: one in csv and one in json. For the above, JSON should be your default representation for the above three basic APIs. 
    * "http://<host:port>/listAll/csv" should return all open and close times in CSV format
    * "http://<host:port>/listOpenOnly/csv" should return open times only in CSV format
    * "http://<host:port>/listCloseOnly/csv" should return close times only in CSV format

    * "http://<host:port>/listAll/json" should return all open and close times in JSON format
    * "http://<host:port>/listOpenOnly/json" should return open times only in JSON format
    * "http://<host:port>/listCloseOnly/json" should return close times only in JSON format

* Query parameter to get top "k" open and close times. For examples, see below.

    * "http://<host:port>/listOpenOnly/csv?top=3" should return top 3 open times only (in ascending order) in CSV format 
    * "http://<host:port>/listOpenOnly/json?top=5" should return top 5 open times only (in ascending order) in JSON format
    * "http://<host:port>/listCloseOnly/csv?top=6" should return top 5 close times only (in ascending order) in CSV format
    * "http://<host:port>/listCloseOnly/json?top=4" should return top 4 close times only (in ascending order) in JSON format

* You'll also design consumer programs (e.g., in jQuery) to use the service that you expose. "website" inside DockerRestAPI is an example of that. It is uses PHP. You're welcome to use either PHP or jQuery to consume your services. NOTE: your consumer program should be in a different container like example in DockerRestAPI.

